# Consumer Electronics - U.S. company

U.S. Consumer Electronics Retailer - Sales Analysis 

In this project, I conducted an exploratory data analysis by using a sales dataset from an American online retail company. This corporation is present in some of the most important cities in the U.S. and it mainly sells electronic produts and accessories, for example, mobile phones, laptops, batteries, among others. This study's goal is to analyze the company's performance in 2019, and to discover opportunities to boost its sales. In a real world situation, this information would be valuable to the firm's strategic decision-making process, because it would allow the directors to know if the corporation's KPIs (Key Performance Indicators) are reaching their predetermined targets.

To execute this project, I used Python (pandas) and Tableau.

Data Source: I downloaded the dataset from Keith Galli's repository: https://github.com/KeithGalli/Pandas-Data-Science-Tasks. 
